def number_par():
    number = int(input('ingresa el numero: '))
    if number % 2 == 0:
        print('the number ', number, 'is even')
    else:
        print('the number ', number, ' is not even ')

def area_triangulo():
    base = float(input('Enter the base of the triangle: '))
    height = float(input('Enter the height of the triangle: '))
    area = (base * height) / 2
    print('El area es igual a: ', area)

def suma_enteros():
    number = int(input('Ingresa el numero entero mayor que 0: '))

    while (number < 1):
        number = int(input('Ingresa el numero entero mayor que 0'))

    suma = (number * (number + 1)) / 2
    print('La sumatoria de los numeros de 1 hasta ', number, ' es ', suma)

if __name__=="__main__":
    n = '1'
    while n == '1':
        print('---------- menu ----------------')
        print('Verificar si un numero es par = 1')
        print('Hallar el area de un triangulo = 2')
        print('Sumatoria de un numero entero hasta n = 3')
        opcion = input()
        if opcion == '1':
            number_par()
        elif opcion == '2':
            area_triangulo()
        elif opcion == '3':
            suma_enteros()
        else:
            print('opción equivocada ')
        print("\n")
        print('si quieres continuar  preciona 1 ')
        print('Para salir cualquier otra tecla')
        n = input()