import random


def show_doll(n):
    toy = [' | ', ' () ', '/|\ ', ' | ', ' | ', '/ \ '
           ]
    for k in range(n):
        print(toy[k])


def lines(n):
    line = []
    while (n > 0):
        line.append('_')
        n -= 1
    return line


def run():
    words = ['amor',
             'esternocleidomastoideo',
             'electroencefalografia',
             'ovoviviparo',
             'python',
             'play',
             'miedo',
             'antibiotico',
             'procedimiento',
             'inteligencia']
    index = random.randint(0, 9)
    word = words[index]
    line = lines(len(word))
    oportunidades = 6
    ganador = 0
    while (oportunidades > 0 and ganador == 0):
        show_doll(oportunidades)
        for i in line:
            print(i, end=" ")
        letter = input(' ingresa la letra: ')
        letter = letter.lower()
        if letter in word:
            lugar = 0
            for i in word:
                if i == letter:
                    line[lugar] = letter
                lugar += 1
        else:
            oportunidades -= 1

        if not ("_" in line):
            print('Felicidades ganaste')
            ganador = 1


if __name__ == "__main__":
    n = '1'
    while n == '1':
        run()
        print('')
        print('si quieres volver a jugar  preciona 1 ')
        print('Para salir cualquier otra tecla')
        n = input()
