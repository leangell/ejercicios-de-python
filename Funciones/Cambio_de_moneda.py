def keys(number):
    if number == 1:
        key = "COP"
    elif number == 2:
        key = "USD"
    elif number == 3:
        key = "EUR"
    elif number == 4:
        key = "GBP"
    elif number == 5:
        key = "YEN"
    return key


def convertir(cantidad, d_origen, d_convertir):
    monedas = {
        "COP": {
            "EUR": 0.0002267,
            "USD": 0.0002555,
            "GBP": 0.0001904,
            "YEN": 0.02953031
        },
        "EUR": {
            "USD": 1.127,
            "GBP": 0.840,
            "YEN": 130.25,
            "COP": 4410.79
        },
        "USD": {
            "COP": 3912.62,
            "EUR": 0.88,
            "GBP": 0.74,
            "YEN": 115
        },
        "GBP": {
            "YEN": 155,
            "USD": 1.34,
            "EUR": 1.19,
            "COP": 5249.24
        },
        "YEN": {
            "EUR": 0.007677,
            "USD": 0.008653,
            "GBP": 0.006450,
            "COP": 33.86
        }
    }
    dinero = monedas[d_origen]
    d_convertido = dinero[d_convertir] * cantidad
    return d_convertido


def maIn():
    amount=0
    key_1=6
    key_2=-1
    while((key_1<1 or key_1>5) or (key_2 < 1 or key_1 > 5) or amount <1 ):
        amount = float(input('Ingrese la cantidad de dinero correcta: '))

        print(" Moneda origen :\n Pesos = 1 \n ""Dolartes = 2\n Euros = 3\n libras esterlinas= 4\n Yens: 5")
        key_1 = int(input('moneda origen '))

        print(" Convertir a :\n Pesos = 1 \n ""Dolartes = 2\n Euros = 3\n libras esterlinas= 4\n Yens: 5 ")
        key_2 = int(input('Moneda a convertir:  '))

        if ((key_1<1 or key_1>5) or (key_2 < 1 or key_1 > 5) or amount <1 ):
            print('Ingresa valores validos ')

    origen = keys(key_1)
    new_val = keys(key_2)
    if origen == new_val:
        print(f'La conversion fue la siguiente {amount} {origen} => {amount} {new_val} ')
    else:
        convertido = convertir(amount, origen, new_val)
        print(f'La conversion fue la siguiente {amount} {origen} => {convertido} {new_val} ')


if __name__ == "__main__":
    n = '1'
    while n == '1':
        maIn()
        print('si quieres continuar  preciona 1 ')
        print('Para salir cualquier otra tecla')
        n = input()
